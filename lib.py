import os
import sys
from stat import *

import fontforge

class MyFont:

    def __init__(self, input_font):
        self.file = input_font
        self.root = os.path.splitext(self.file)[0]
        self.ff = fontforge.open(input_font)

    def to_codepoints(self):
        for glyph in self.ff.glyphs():
            print "{0:x}".format(glyph.unicode)

class InterpolateFonts:

    """Produce fonts using the interpolateFonts method of FontForge.

    The kern of this class is the interpolate method.
    
    FontForge defines an `interpolateFonts' method for its font type. The
    amount of blending is defined with a `fraction'. From the FontForge
    documentation:
    
    "If you are interpolating from a light font to a bold one, then a medium
    font might be 50% between the two, an extra-bold font might be 200% and a
    thin one -100%."
    
    http://fontforge.sourceforge.net/elementmenu.html#Interpolate

    The three different `parameters' correspond to the three types of
    interpolation that are alluded to in the FontForge documentation.
    
    For example, if the `steps' attribute defines a progression of four fonts,
    and parameter is `x' then the following fonts are made
    from_font.interpolateFonts(0, to_font)
    from_font.interpolateFonts(0.25, to_font)
    from_font.interpolateFonts(0.5, to_font)
    from_font.interpolateFonts(0.75, to_font)
    from_font.interpolateFonts(1, to_font)

    """

    def __init__(self, from_font, to_font, parameter, steps):
        """The from_font is the `initial' font.
        The to_font is the `final' font.
        The `steps' determine the number of fonts to be generated.
        `parameter' determines the type of interpolation.

        """
        self.from_font = from_font
        self.to_font = to_font
        self.parameter = parameter
        self.steps = steps

    def font_info(self):
        """Define strings for file and font names based on the input fonts."""
        self.directory = os.path.split(self.from_font)[1] + os.path.split(self.to_font)[1] + self.parameter.upper()
        self.fromfont_noextension = os.path.splitext(os.path.split(self.from_font)[1])[0]
        self.tofont_noextension = os.path.splitext(os.path.split(self.to_font)[1])[0]
        self.filename = self.fromfont_noextension + " to " + self.tofont_noextension + " " + self.parameter.upper()
        self.familyname = self.fullname = self.fromfont_noextension + " to " + self.tofont_noextension
        self.fontname = self.fromfont_noextension + "to" + self.tofont_noextension + "-" + self.parameter.upper()
        self.copyright = "put together by " + sys.argv[0] + " from " + os.path.split(self.from_font)[1] + " and " + os.path.split(self.to_font)[1]

    def interpolate(self):
        """Produce a series of fonts using the interpolateFonts method.

        The fonts are created in a directory whose name is defined in the
        font_info method. Likewise for the file names, family names, etc.
        
        The try/except checks whether the target directory exists already. 

        The number of iterations in the main loop is dependent on the `steps'
        attribute.

        """
        try:
            os.mkdir(self.directory)
        except OSError:
            pass

        self.from_font_ff = fontforge.open(self.from_font)

        myfraction = 1.0 / self.steps

        os.chdir(self.directory)

        for mystep in range(self.steps):

            if self.parameter == "x":
                myfont = self.from_font_ff.interpolateFonts(myfraction * mystep, self.to_font)
            elif self.parameter == "y":
                myfont = self.from_font_ff.interpolateFonts(-1 * myfraction * mystep, self.to_font)
            elif self.parameter == "z":
                myfont = self.from_font_ff.interpolateFonts(1 + myfraction * mystep, self.to_font)

            myfont.familyname = self.familyname
            myfont.fontname = self.fontname + str(int(myfraction * mystep * self.steps)).zfill(5)
            myfont.fullname = self.fullname + str(int(myfraction * mystep * self.steps)).zfill(5)
            myfont.copyright = self.copyright
            myfont.generate(self.filename + str(int(myfraction * mystep * self.steps)).zfill(5) + ".ttf")
    
        if self.parameter == "x":
            myfont = self.from_font_ff.interpolateFonts(1, self.to_font)
        elif self.parameter == "y":
            myfont = self.from_font_ff.interpolateFonts(-1, self.to_font)
        elif self.parameter == "z":
            myfont = self.from_font_ff.interpolateFonts(2, self.to_font)

        myfont.familyname = self.familyname
        myfont.fontname = self.fontname + str(self.steps).zfill(5)
        myfont.fullname = self.fullname + str(self.steps).zfill(5)
        myfont.copyright = self.copyright
        myfont.generate(self.filename + str(self.steps).zfill(5) + ".ttf")

        os.chdir("..")

class InterpolationPathsWrapper:

    """A font file or a directory of font files may be specified on the cli.

    This could give rise to permutations.

    This class is called by the command line script. The two paths are passed
    directly from the command line to the my_paths tuple and to this class.
        
    The extra `parameter' argument determines the type of interpolation as
    defined in the InterpolateFonts class.
        
    We use the stat module to tell files and directories apart
    http://docs.python.org/library/stat.html

    """

    def __init__(self, path1, path2, my_parameter, steps): 
        """path1 is the `initial' font or directory of fonts.
        path1 is the `final' font or directory of fonts.
        The `steps' determine the number of fonts to be generated.
        `my_parameter' determines the type of interpolation.

        """
        self.path1 = path1
        self.path2 = path2
        self.my_parameter = my_parameter
        self.steps = steps

    def interpolate(self):
        """Interpolate fonts after the `binary operation' functions."""
        mode1 = os.stat(self.path1)[ST_MODE]
        mode2 = os.stat(self.path2)[ST_MODE]
    
        if S_ISDIR(mode1) and S_ISDIR(mode2):
            # Both locations are directories
            self.directory_times_directory()
        elif S_ISREG(mode1) and S_ISREG(mode2):
            # Both locations are files
            self.font_times_font()
        elif S_ISREG(mode1):
            # self.path1 is a file
            self.font_times_directory()
        elif S_ISREG(mode2):
            # self.path2 is a file
            self.directory_times_font()

    def font_times_font(self):
        """Interface to the main method of InterpolateFonts.
    
        In turn, this method is an interface to the interpolate method of the
        fontforge font type.
    
        self.path1 and self.path2 are font files on disk.
    
        The `binary operations' that follow are in principle based on this
        function, ie font_times_directory, etc.
    
        """
        myfont1_times_myfont2 = InterpolateFonts(self.path1, self.path2, self.my_parameter, self.steps)
        myfont1_times_myfont2.font_info()
        myfont1_times_myfont2.interpolate()
    
    def font_times_directory(self):
        """Interpolates self.path1 with all the fonts in self.path2."""
        for dirpath, dirnames, filenames in os.walk(self.path2):
            for font2 in filenames:
                myfont1_times_myfont2 = InterpolateFonts(self.path1, os.path.join(dirpath, font2), self.my_parameter, self.steps)
                myfont1_times_myfont2.font_info()
                myfont1_times_myfont2.interpolate()
    
    def directory_times_font(self):
        """Interpolates all the fonts in self.path1 with self.path2."""
        for dirpath, dirnames, filenames in os.walk(self.path1):
            for font1 in filenames:
                myfont1_times_myfont2 = InterpolateFonts(os.path.join(dirpath, font1), self.path2, self.my_parameter, self.steps)
                myfont1_times_myfont2.font_info()
                myfont1_times_myfont2.interpolate()
    
    def directory_times_directory(self):
        """Interpolates all the fonts in self.path1 with all the fonts in self.path2."""
        for dirpath0, dirnames0, filenames0 in os.walk(self.path1):
            for myfont1 in filenames0:
                for dirpath1, dirnames1, filenames1 in os.walk(self.path2):
                    for myfont2 in filenames1:
                        myfont1_times_myfont2 = InterpolateFonts(os.path.join(dirpath0, myfont1), os.path.join(dirpath1, myfont2), self.my_parameter, self.steps)
                        myfont1_times_myfont2.font_info()
                        myfont1_times_myfont2.interpolate()
