#!/usr/bin/env python

from optparse import OptionParser
import os

import lib

def handle_options(my_options):
    """Process optparse options with the InterpolateFonts class in mind.

    One flag is passed directly from the command line to my_options.

    The x, y, z flags correspond to the `parameter' attribute of the
    InterpolateFonts class, cf its docstring

    http://fontforge.sourceforge.net/elementmenu.html#Interpolate
    Examples: If you are interpolating from a light font to a bold one, then a
    medium font might be 50% between the two, an extra-bold font might be 200%
    and a thin one -100%.

    """
    if my_options.x:
        my_parameter = "x"
    elif my_options.y:
        my_parameter = "y"
    elif my_options.z:
        my_parameter = "z"

    return my_parameter

if __name__ == "__main__":

    usage = "%prog [-xyz] -o number font_path1 font_path2"
    epi = """Interpolate between two font paths and generate a number of ttf
files along the way. For permutations, the font paths may be directories.
Fontforge defines three types of interpolation. We call them x, y, and z. This
software produces crashes, freezes, segmentation faults and fonts."""

    gui = OptionParser(usage, epilog=epi)
    gui.add_option("-o", "--output", action="store", type="int",
            dest="output_files", help="positive integer related to amount of font production")
    gui.add_option("-x", action="store_true", dest="x", help="interpolation 100%")
    gui.add_option("-y", action="store_true", dest="y", help="interpolation -100%")
    gui.add_option("-z", action="store_true", dest="z", help="interpolation 200%")
    (options, paths) = gui.parse_args()

    a_parameter = handle_options(options)

    wrap_interpolation_paths = lib.InterpolationPathsWrapper(paths[0], paths[1], a_parameter, options.output_files)
    wrap_interpolation_paths.interpolate()
